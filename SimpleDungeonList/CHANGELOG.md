# v1.0.0 (2020-05-07)
- Features:
  - Simple list with all dungeons (and raids)
  - Colored rows depending on your character level and the last boss level (or your level and dungeon level average)
  - Include/exclude information on each row: minimum level to enter, level range, dungeon abbreviation, and last boss level information for each dungeon
  - Changeable row colors, font size & family
  - Debug options where you can change the current level and see the list update depending on what you choose (don't forget to turn on debug mode!)
  - Minimap icon!
