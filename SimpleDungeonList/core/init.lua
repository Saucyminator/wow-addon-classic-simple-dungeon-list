SDL = LibStub("AceAddon-3.0"):GetAddon("SDL", true) or LibStub("AceAddon-3.0"):NewAddon("SDL");
LibStub("AceAddon-3.0"):EmbedLibraries(SDL, "AceConsole-3.0", "AceEvent-3.0");

local addonName = ...;
SDL.addonName = addonName;
SDL.addonTitle = GetAddOnMetadata(addonName, "Title");
SDL.author	= GetAddOnMetadata(addonName, "Author");
SDL.version	= GetAddOnMetadata(addonName, "Version");
SDL.playerName =  UnitName("player");
SDL.playerLevel = UnitLevel("player");

SDL.about = {};
SDL.knownIssues = {};
SDL.changelog = {};
SDL.defaults = {};
SDL.options = {};
SDL.data = {};
