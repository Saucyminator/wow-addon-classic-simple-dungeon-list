SDL.Localization = LibStub("AceLocale-3.0"):GetLocale("SDL", true);


-- about
SDL.about = {
  website = "",
  issues = "https://gitlab.com/Saucyminator/wow-addon-classic-simple-dungeon-list/issues"
}


-- known issues
SDL.knownIssues = [[Q: Window position isn't saved.
A: Window position is saved whenever you close the window. I haven't figured out how to save the position when user has stopped draging the window.

Q: Level colors aren't being reset.
A: The GUI for the colors aren't being updated properly but the default color values are correctly set in the SavedVariables. Please do a /reload to show the correct colors after doing a reset.

Q: Why is only English supported?
A: I don't know any other language than English. You are more than welcomed to help me translate more languages. Create a new issue (follow the link below) or submit a pull request.

Q: Why are some bosses level 63?
A: Level 63 is the "skull" level.]];


-- changelog
SDL.changelog = [[v1.0.0 (2020-05-07)
- Features:
  - Simple list with all dungeons (and raids)
  - Colored rows depending on your character level and the last boss level (or your level and dungeon level average)
  - Include/exclude information on each row: minimum level to enter, level range, dungeon abbreviation, and last boss level information for each dungeon
  - Changeable row colors, font size & family
  - Debug options where you can change the current level and see the list update depending on what you choose (don't forget to turn on debug mode!)
  - Minimap icon!]];


-- config
SDL.defaults = {
  global = {
    debug = {
      debugMode = false,
      debugMessages = false,
      debugLevel = 30,
      debugColors = {
        gray = { red = 0.5, green = 0.5, blue = 0.5},
        green = { red = 0, green = 0.5, blue = 0}
      }
    },
    theme = {
      commands = { red = 0, green = 0.8, blue = 1, hex = "00ccff" },
      warning = { red = 1, green = 0, blue = 0, hex = "ff0000" },
      minimap = { red = 1, green = 1, blue = 1, hex = "ffffff" },
      enabled = { red = 0, green = 1, blue = 0, hex = "00ff00" },
      disabled = { red = 1, green = 0, blue = 0, hex = "ff0000" }
    },
    frame = {
      scale = 1,
      width = 500,
      height = 530,
	    -- locked = false,
	    position = {"LEFT", "UIParent", "LEFT", 50, 0}
    },
    font = {
      family = "Friz Quadrata TT",
      size = 12
    }
  },
  profile = {
    general = {
      loadedWelcomeMessage = true, -- display welcome message
      loadedStartShown = false, -- display at start
      colorText = true, -- display colors for each row
      colorsBasedOnLastBossLevel = true, -- calculate colors based on last boss level
      colors = {
        gray = { red = 0.5, green = 0.5, blue = 0.5, hex = "808080"},
        green = { red = 0, green = 0.5, blue = 0, hex = "008000"},
        yellow = { red = 1, green = 1, blue = 0, hex = "ffff00"},
        orange = { red = 1, green = 0.65, blue = 0, hex = "ffa500"},
        red = { red = 0.86, green = 0.08, blue = 0.24, hex = "dc143c"},
        boss = { red = 1, green = 0, blue = 0, hex = "ff0000"}
      },
      textAbbreviation = true,
      textLastBossLevel = true, -- display last boss' level
      textMinimumLevel = true, -- display minimum level to enter
      textRecommendedLevels = true, -- display recommended levels
      displayRaids = false, -- display raid dungeons
      sounds = true
    },
    minimap = {
      hide = false
    }
  }
}

-- options
SDL.options = {
	name = SDL.addonName,
  handler = SDL,
  type = "group",
  childGroups = "tab",
	args = {
    intro = {
      order = 0,
      name = string.format(SDL.Localization["settings description"], SDL.version, SDL.about.issues),
      type = "description"
    },

		general_tab = {
			order = 1,
			name = SDL.Localization["settings tab general"],
			type = "group",
			args = {
        headerLoaded = {
          order = 8,
          name = SDL.Localization["settings general loaded header"],
					type = "header",
        },
        loadedWelcomeMessage = {
          order = 9,
          name = SDL.Localization["settings general loaded welcome message"],
          desc = SDL.Localization["settings general loaded welcome message desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.loadedWelcomeMessage
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.loadedWelcomeMessage = value
          end
        },
        loadedStartShown = {
          order = 10,
          name = SDL.Localization["settings general loaded start shown"],
          desc = SDL.Localization["settings general loaded start shown desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.loadedStartShown
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.loadedStartShown = value
          end
        },
        headerColors = {
          order = 11,
          name = SDL.Localization["settings general colors header"],
					type = "header",
        },
        colorText = {
          order = 12,
          name = SDL.Localization["settings general colors row"],
          desc = SDL.Localization["settings general colors row desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.colorText;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.colorText = value;

            SDL:UpdateFrame();
          end
        },
        colorsBasedOnLastBossLevel = {
          order = 13,
          name = SDL.Localization["settings general colors based on last boss level"],
          desc = SDL.Localization["settings general colors based on last boss level desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.colorsBasedOnLastBossLevel;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.colorsBasedOnLastBossLevel = value;

            SDL:UpdateFrame();
          end
        },
        colors = {
          order = 14,
          name = SDL.Localization["settings general colors level colors"],
          desc = SDL.Localization["settings general colors level colors desc"],
          type = "group",
          inline = true,
          get = function (info)
            local color = {
              SDL.db.profile.general.colors[info[3]].red,
              SDL.db.profile.general.colors[info[3]].green,
              SDL.db.profile.general.colors[info[3]].blue,
              1
            }

            return unpack(color);
          end,
          set = function (info, red, green, blue)
            SDL:DebugSettings(info, {red, green, blue});

            SDL.db.profile.general.colors[info[3]].red = red;
            SDL.db.profile.general.colors[info[3]].green = green;
            SDL.db.profile.general.colors[info[3]].blue = blue;

            SDL:UpdateFrame();
          end,
          args = {
            gray = {
              order = 1,
              name = SDL.Localization["settings general colors gray"],
              desc = SDL.Localization["settings general colors gray desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            green = {
              order = 3,
              name = SDL.Localization["settings general colors green"],
              desc = SDL.Localization["settings general colors green desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            yellow = {
              order = 5,
              name = SDL.Localization["settings general colors yellow"],
              desc = SDL.Localization["settings general colors yellow desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            orange = {
              order = 2,
              name = SDL.Localization["settings general colors orange"],
              desc = SDL.Localization["settings general colors orange desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            red = {
              order = 4,
              name = SDL.Localization["settings general colors red"],
              desc = SDL.Localization["settings general colors red desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            boss = {
              order = 6,
              name = SDL.Localization["settings general colors boss"],
              desc = SDL.Localization["settings general colors boss desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            }
          }
        },
        resetColors = {
          order = 15,
          name = SDL.Localization["settings general colors reset"],
          desc = SDL.Localization["settings general colors reset desc"],
          type = "execute",
          func = function(info, value)
            SDL:DebugSettings(info, SDL.Localization["settings general colors reset"]);

            SDL:ResetColors();

            SDL:UpdateFrame();
          end
        },
        headerText = {
          order = 16,
          name = SDL.Localization["settings general text header"],
          type = "header",
        },
        textMinimumLevel = {
          order = 17,
          name = SDL.Localization["settings general text minimum level"],
          desc = SDL.Localization["settings general text minimum level desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.textMinimumLevel
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.textMinimumLevel = value;

            SDL:UpdateFrame();
          end
        },
        textRecommendedLevels = {
          order = 18,
          name = SDL.Localization["settings general text recommended levels"],
          desc = SDL.Localization["settings general text recommended levels desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.textRecommendedLevels
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.textRecommendedLevels = value;

            SDL:UpdateFrame();
          end
        },
        textAbbreviation = {
          order = 19,
          name = SDL.Localization["settings general text abbreviation"],
          desc = SDL.Localization["settings general text abbreviation desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.textAbbreviation
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.textAbbreviation = value;

            SDL:UpdateFrame();
          end
        },
        textLastBossLevel = {
          order = 20,
          name = SDL.Localization["settings general text last boss level"],
          desc = SDL.Localization["settings general text last boss level desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.textLastBossLevel
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.textLastBossLevel = value;

            SDL:UpdateFrame();
          end
        },
        headerDisplay = {
          order = 21,
          name = SDL.Localization["settings general display rows header"],
          type = "header",
        },
        displayRaids = {
          order = 22,
          name = SDL.Localization["settings general display raids"],
          desc = SDL.Localization["settings general display raids desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SDL.db.profile.general.displayRaids
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.displayRaids = value;

            SDL:UpdateFrame();
          end
        },
        headerSounds = {
          order = 23,
          name = SDL.Localization["settings general sounds header"],
          type = "header",
        },
        sounds = {
          order = 24,
          name = SDL.Localization["settings general sounds"],
          desc = SDL.Localization["settings general sounds desc"],
          type = "toggle",
          width = 1.5,
          get = function ()
            return SDL.db.profile.general.sounds
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.profile.general.sounds = value;
          end
        },
        headerFont = {
          order = 25,
          name = SDL.Localization["settings general font header"],
          type = "header",
        },
        fontFamily = {
          order = 26,
          name = SDL.Localization["settings general font size"],
          desc = SDL.Localization["settings general font size desc"],
          type = "select",
          width = 1.5,
          dialogControl = "LSM30_Font",
          values = AceGUIWidgetLSMlists.font,
          get = function ()
            return SDL.db.global.font.family;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.global.font.family = value;

            SDL:UpdateFrame();
          end
        },
        fontSize = {
          order = 27,
          name = SDL.Localization["settings general font size"],
          desc = SDL.Localization["settings general font size desc"],
          type = "range",
          width = 1.5,
          min = 7,
          max = 24,
          step = 1,
          get = function ()
            return SDL.db.global.font.size;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.global.font.size = value;

            SDL:UpdateFrame();
          end
        },
        headerMinimap = {
          order = 28,
          name = SDL.Localization["settings general minimap header"],
					type = "header"
        },
				toggleShown = {
					order = 29,
          name = SDL.Localization["settings general minimap"],
          desc = SDL.Localization["settings general minimap desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return not SDL.db.profile.minimap.hide;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL:Minimap();
          end
        }

      }
    },

    about_tab = {
      order = 2,
      name = SDL.Localization["settings tab about"],
      type = "group",
      args = {
				labelAbout = {
					order = 2,
          name = string.format(SDL.Localization["settings about"], SDL.author),
          desc = SDL.Localization["settings about desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
        headerKnownIssues = {
          order = 10,
          name = SDL.Localization["settings about known issues header"],
					type = "header"
        },
				labelKnownIssues = {
					order = 11,
          name = string.format(SDL.Localization["settings about known issues"], SDL.knownIssues),
          desc = SDL.Localization["settings about known issues desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
        headerIssues = {
          order = 20,
          name = SDL.Localization["settings about issues header"],
					type = "header"
        },
				labelIssues = {
					order = 21,
          name = string.format(SDL.Localization["settings about issues"], SDL.about.issues),
          desc = SDL.Localization["settings about issues desc"],
					type = "description",
					width = "full",
          fontSize = "medium"
        },
        popup = {
          order = 22,
          name = SDL.Localization["settings about issues button report"],
          desc = SDL.Localization["settings about issues button report desc"],
          type = "execute",
          width = 1,
          func = function (info, value)
            SDL:Popup()
          end
        }
      }
    },

		debug_tab = {
			order = 99,
			name = SDL.Localization["settings tab debug"],
			type = "group",
			args = {
				debugHeader = {
					order = 1,
					name = SDL.Localization["settings debug header"],
					type = "header",
				},
				debugMode = {
					order = 2,
          name = SDL.Localization["settings debug mode"],
          desc = SDL.Localization["settings debug mode desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return SDL.db.global.debug.debugMode;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.global.debug.debugMode = value;

            SDL:UpdateFrame();
          end
        },
				debugMessages = {
					order = 3,
          name = SDL.Localization["settings debug chat messages"],
          desc = SDL.Localization["settings debug chat messages desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return SDL.db.global.debug.debugMessages;
          end,
          set = function (info, value)
            SDL:DebugSettings(info, value);

            SDL.db.global.debug.debugMessages = value;

            SDL:UpdateFrame();
          end
        },
        debugLevel = {
          order = 3,
          name = SDL.Localization["settings debug test level range"],
          desc = SDL.Localization["settings debug test level range desc"],
          type = "range",
					width = "full",
          min = 1,
          max = 60,
          step = 1,
          get = function ()
            return SDL.db.global.debug.debugLevel;
          end,
          set = function(info, value)
            SDL:DebugSettings(info, value);

            SDL.db.global.debug.debugLevel = value;

            SDL:UpdateFrame();
          end
        },
				debugResetHeader = {
					order = 98,
					name = SDL.Localization["settings debug reset header"],
					type = "header",
				},
        debugResetButton = {
          order = 99,
          name = SDL.Localization["settings debug reset"],
          desc = SDL.Localization["settings debug reset desc"],
          type = "execute",
					width = "full",
          func = function(info, value)
            SDL:DebugSettings(info, value);

            SDL:Reset();
          end
        },
        debugColors = {
          order = 100,
          name = "debug color",
          type = "group",
          inline = true,
          get = function (info)
            local color = {
              SDL.db.global.debug.debugColors[info[3]].red,
              SDL.db.global.debug.debugColors[info[3]].green,
              SDL.db.global.debug.debugColors[info[3]].blue,
            }

            return unpack(color);
          end,
          set = function (info, red, green, blue)
            SDL:DebugSettings(info, {red, green, blue});

            SDL.db.global.debug.debugColors[info[3]].red = red;
            SDL.db.global.debug.debugColors[info[3]].green = green;
            SDL.db.global.debug.debugColors[info[3]].blue = blue;
          end,
          args = {
            gray = {
              order = 1,
              name = "debug gray",
              desc = "",
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            green = {
              order = 3,
              name = "debug green",
              desc = "",
              type = "color",
              width = 1.5,
              hasAlpha = false
            }
          }
        },
        debugResetColors = {
          order = 101,
          name = SDL.Localization["settings general colors reset"],
          desc = SDL.Localization["settings general colors reset desc"],
          type = "execute",
          func = function(info, value)
            SDL:DebugSettings(info, SDL.Localization["settings general colors reset"]);

            for k,v in pairs(SDL.defaults.global.debug.debugColors) do
              SDL.db.global.debug.debugColors[k] = v;
              print(k, v)
            end
          end
        },
			}
    },

    changelog_tab = {
      order = 100,
			name = SDL.Localization["settings tab changelog"],
			type = "group",
			args = {
				labelChangelog = {
					order = 1,
          name = string.format(SDL.Localization["settings changelog"], SDL.changelog),
          desc = SDL.Localization["settings changelog desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
      }
    }
	}
}

-- data
SDL.data = {
  -- Dungeons
  {
    order = 1,
    minimum = 8,
    minLevel = 13,
    maxLevel = 18,
    lastBossLevel = 16,
    name = SDL.Localization["dungeons rfc"],
    abbr = SDL.Localization["dungeons rfc abbr"],
    location = SDL.Localization["dungeons rfc location"],
    raid = false
  },
  {
    order = 2,
    minimum = 10,
    minLevel = 17,
    maxLevel = 24,
    lastBossLevel = 22,
    name = SDL.Localization["dungeons wc"],
    abbr = SDL.Localization["dungeons wc abbr"],
    location = SDL.Localization["dungeons wc location"],
    raid = false
  },
  {
    order = 3,
    minimum = 10,
    minLevel = 17,
    maxLevel = 24,
    lastBossLevel = 21,
    name = SDL.Localization["dungeons dm"],
    abbr = SDL.Localization["dungeons dm abbr"],
    location = SDL.Localization["dungeons dm location"],
    raid = false
  },
  {
    order = 4,
    minimum = 10,
    minLevel = 22,
    maxLevel = 30,
    lastBossLevel = 26,
    name = SDL.Localization["dungeons sfk"],
    abbr = SDL.Localization["dungeons sfk abbr"],
    location = SDL.Localization["dungeons sfk location"],
    raid = false
  },
  {
    order = 5,
    minimum = 15,
    minLevel = 24,
    maxLevel = 32,
    lastBossLevel = 28,
    name = SDL.Localization["dungeons bfd"],
    abbr = SDL.Localization["dungeons bfd abbr"],
    location = SDL.Localization["dungeons bfd location"],
    raid = false
  },
  {
    order = 6,
    minimum = 15,
    minLevel = 24,
    maxLevel = 30,
    lastBossLevel = 29,
    name = SDL.Localization["dungeons stockade"],
    abbr = SDL.Localization["dungeons stockade abbr"],
    location = SDL.Localization["dungeons stockade location"],
    raid = false
  },
  {
    order = 7,
    minimum = 19,
    minLevel = 29,
    maxLevel = 38,
    lastBossLevel = 34,
    name = SDL.Localization["dungeons gnomer"],
    abbr = SDL.Localization["dungeons gnomer abbr"],
    location = SDL.Localization["dungeons gnomer location"],
    raid = false
  },
  {
    order = 8,
    minimum = 19,
    minLevel = 29,
    maxLevel = 38,
    lastBossLevel = 33,
    name = SDL.Localization["dungeons rfk"],
    abbr = SDL.Localization["dungeons rfk abbr"],
    location = SDL.Localization["dungeons rfk location"],
    raid = false
  },
  {
    order = 9,
    minimum = 20,
    minLevel = 29,
    maxLevel = 36,
    lastBossLevel = 34,
    name = SDL.Localization["dungeons sm gy"],
    abbr = SDL.Localization["dungeons sm gy abbr"],
    location = SDL.Localization["dungeons sm gy location"],
    raid = false
  },
  {
    order = 10,
    minimum = 20,
    minLevel = 32,
    maxLevel = 39,
    lastBossLevel = 37,
    name = SDL.Localization["dungeons sm lib"],
    abbr = SDL.Localization["dungeons sm lib abbr"],
    location = SDL.Localization["dungeons sm lib location"],
    raid = false
  },
  {
    order = 11,
    minimum = 20,
    minLevel = 36,
    maxLevel = 43,
    lastBossLevel = 40,
    name = SDL.Localization["dungeons sm arms"],
    abbr = SDL.Localization["dungeons sm arms abbr"],
    location = SDL.Localization["dungeons sm arms location"],
    raid = false
  },
  {
    order = 12,
    minimum = 20,
    minLevel = 38,
    maxLevel = 45,
    lastBossLevel = 42,
    name = SDL.Localization["dungeons sm cath"],
    abbr = SDL.Localization["dungeons sm cath abbr"],
    location = SDL.Localization["dungeons sm cath location"],
    raid = false
  },
  {
    order = 13,
    minimum = 25,
    minLevel = 37,
    maxLevel = 46,
    lastBossLevel = 41,
    name = SDL.Localization["dungeons rfd"],
    abbr = SDL.Localization["dungeons rfd abbr"],
    location = SDL.Localization["dungeons rfd location"],
    raid = false
  },
  {
    order = 14,
    minimum = 30,
    minLevel = 41,
    maxLevel = 51,
    lastBossLevel = 47,
    name = SDL.Localization["dungeons ulda"],
    abbr = SDL.Localization["dungeons ulda abbr"],
    location = SDL.Localization["dungeons ulda location"],
    raid = false
  },
  {
    order = 15,
    minimum = 35,
    minLevel = 44,
    maxLevel = 54,
    lastBossLevel = 48,
    name = SDL.Localization["dungeons zf"],
    abbr = SDL.Localization["dungeons zf abbr"],
    location = SDL.Localization["dungeons zf location"],
    raid = false
  },
  {
    order = 16,
    minimum = 30,
    minLevel = 46,
    maxLevel = 55,
    lastBossLevel = 48,
    name = SDL.Localization["dungeons mara orange"],
    abbr = SDL.Localization["dungeons mara orange abbr"],
    location = SDL.Localization["dungeons mara orange location"],
    raid = false
  },
  {
    order = 17,
    minimum = 30,
    minLevel = 46,
    maxLevel = 55,
    lastBossLevel = 47,
    name = SDL.Localization["dungeons mara purple"],
    abbr = SDL.Localization["dungeons mara purple abbr"],
    location = SDL.Localization["dungeons mara purple location"],
    raid = false
  },
  {
    order = 18,
    minimum = 30,
    minLevel = 46,
    maxLevel = 55,
    lastBossLevel = 49,
    name = SDL.Localization["dungeons mara poison falls"],
    abbr = SDL.Localization["dungeons mara poison falls abbr"],
    location = SDL.Localization["dungeons mara poison falls location"],
    raid = false
  },
  {
    order = 19,
    minimum = 30,
    minLevel = 46,
    maxLevel = 54,
    lastBossLevel = 51,
    name = SDL.Localization["dungeons mara inner"],
    abbr = SDL.Localization["dungeons mara inner abbr"],
    location = SDL.Localization["dungeons mara inner location"],
    raid = false
  },
  {
    order = 20,
    minimum = 35,
    minLevel = 50,
    maxLevel = 60,
    lastBossLevel = 55,
    name = SDL.Localization["dungeons st"],
    abbr = SDL.Localization["dungeons st abbr"],
    location = SDL.Localization["dungeons st location"],
    raid = false
  },
  {
    order = 21,
    minimum = 40,
    minLevel = 52,
    maxLevel = 60,
    lastBossLevel = 59,
    name = SDL.Localization["dungeons brd"],
    abbr = SDL.Localization["dungeons brd abbr"],
    location = SDL.Localization["dungeons brd location"],
    raid = false
  },
  {
    order = 22,
    minimum = 45,
    minLevel = 55,
    maxLevel = 60,
    lastBossLevel = 60,
    name = SDL.Localization["dungeons lbrs"],
    abbr = SDL.Localization["dungeons lbrs abbr"],
    location = SDL.Localization["dungeons lbrs location"],
    raid = false
  },
  {
    order = 23,
    minimum = 45,
    minLevel = 58,
    maxLevel = 60,
    lastBossLevel = 62,
    name = SDL.Localization["dungeons ubrs"],
    abbr = SDL.Localization["dungeons ubrs abbr"],
    location = SDL.Localization["dungeons ubrs location"],
    raid = false
  },
  {
    order = 24,
    minimum = 45,
    minLevel = 56,
    maxLevel = 60,
    lastBossLevel = 58,
    name = SDL.Localization["dungeons dm east"],
    abbr = SDL.Localization["dungeons dm east abbr"],
    location = SDL.Localization["dungeons dm east location"],
    raid = false
  },
  {
    order = 25,
    minimum = 45,
    minLevel = 56,
    maxLevel = 60,
    lastBossLevel = 62,
    name = SDL.Localization["dungeons dm north"],
    abbr = SDL.Localization["dungeons dm north abbr"],
    location = SDL.Localization["dungeons dm north location"],
    raid = false
  },
  {
    order = 26,
    minimum = 45,
    minLevel = 56,
    maxLevel = 60,
    lastBossLevel = 61,
    name = SDL.Localization["dungeons dm west"],
    abbr = SDL.Localization["dungeons dm west abbr"],
    location = SDL.Localization["dungeons dm west location"],
    raid = false
  },
  {
    order = 27,
    minimum = 45,
    minLevel = 58,
    maxLevel = 60,
    lastBossLevel = 61,
    name = SDL.Localization["dungeons scholo"],
    abbr = SDL.Localization["dungeons scholo abbr"],
    location = SDL.Localization["dungeons scholo location"],
    raid = false
  },
  {
    order = 28,
    minimum = 45,
    minLevel = 58,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons strat living"],
    abbr = SDL.Localization["dungeons strat living abbr"],
    location = SDL.Localization["dungeons strat living location"],
    raid = false
  },
  {
    order = 29,
    minimum = 45,
    minLevel = 58,
    maxLevel = 60,
    lastBossLevel = 62,
    name = SDL.Localization["dungeons strat undead"],
    abbr = SDL.Localization["dungeons strat undead abbr"],
    location = SDL.Localization["dungeons strat undead location"],
    raid = false
  },

  -- Raids
  {
    order = 30,
    minimum = 50,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons ony"],
    abbr = SDL.Localization["dungeons ony abbr"],
    location = SDL.Localization["dungeons ony location"],
    raid = true
  },
  {
    order = 31,
    minimum = 60,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons zg"],
    abbr = SDL.Localization["dungeons zg abbr"],
    location = SDL.Localization["dungeons zg location"],
    raid = true
  },
  {
    order = 32,
    minimum = 58,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons mc"],
    abbr = SDL.Localization["dungeons mc abbr"],
    location = SDL.Localization["dungeons mc location"],
    raid = true
  },
  {
    order = 33,
    minimum = 60,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons bwl"],
    abbr = SDL.Localization["dungeons bwl abbr"],
    location = SDL.Localization["dungeons bwl location"],
    raid = true
  },
  {
    order = 34,
    minimum = 60,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons aq20"],
    abbr = SDL.Localization["dungeons aq20 abbr"],
    location = SDL.Localization["dungeons aq20 location"],
    raid = true
  },
  {
    order = 35,
    minimum = 60,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons aq40"],
    abbr = SDL.Localization["dungeons aq40 abbr"],
    location = SDL.Localization["dungeons aq40 location"],
    raid = true
  },
  {
    order = 36,
    minimum = 60,
    minLevel = 60,
    maxLevel = 60,
    lastBossLevel = 63,
    name = SDL.Localization["dungeons naxx"],
    abbr = SDL.Localization["dungeons naxx abbr"],
    location = SDL.Localization["dungeons naxx location"],
    raid = true
  }
}
