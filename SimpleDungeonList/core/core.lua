-- TODO: Fix: Reset Colors not working. The button works but the graphical update only happens once and not every time I change color and reset it.
-- TODO: Add: Window can locked
-- TODO: Add: Debug window with buttons to test different inputs, like /sdl, /sdl help, etc...

-- init
local AceGUI = LibStub("AceGUI-3.0");
local LSM = LibStub("LibSharedMedia-3.0");
local LDB = LibStub("LibDataBroker-1.1");
local LDBIcon = LibStub("LibDBIcon-1.0");
local isWindowShown = false;


-- functions
function SDL:OnInitialize()
  -- setup database
  self.db = LibStub("AceDB-3.0"):New("SaucyDB", SDL.defaults, true);

  -- setup the frame
  SDL:SetupFrame();

  -- register interface options
  LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable(SDL.addonName, SDL.options, nil);
  LibStub("AceConfigDialog-3.0"):AddToBlizOptions(SDL.addonName, SDL.addonName);

  -- setup minimap
  local addonLDB = LDB:NewDataObject(SDL.addonName, {
    type = "data source",
    text = SDL.addonName,
    icon = "Interface\\Icons\\ability_spy", -- NOTE: Custom way to do it: "Interface\\AddOns\\<ADDON>\\<TEXTURE>"
    OnClick = function(self, button)
      if button == "LeftButton" then
        SDL:ToggleWindow();
      elseif button == "RightButton" then
        SDL:OpenOptions();
      end
    end,
    OnTooltipShow = function(tooltip)
      tooltip:AddDoubleLine(string.format(SDL.Localization["format color"], SDL.db.global.theme.minimap.hex, SDL.addonTitle), string.format(SDL.Localization["format color"], SDL.db.global.theme.minimap.hex, SDL.version));
      tooltip:AddLine(" ");
      tooltip:AddLine(string.format(SDL.Localization["format color value"], SDL.Localization["left click"], SDL.db.global.theme.minimap.hex, SDL.Localization["settings general minimap tooltip left click"]));
      tooltip:AddLine(string.format(SDL.Localization["format color value"], SDL.Localization["right click"], SDL.db.global.theme.minimap.hex, SDL.Localization["settings general minimap tooltip right click"]));
      tooltip:AddLine(string.format(SDL.Localization["format color value"], SDL.Localization["drag"], SDL.db.global.theme.minimap.hex, SDL.Localization["settings general minimap tooltip drag"]));
    end
  });
  LDBIcon:Register(SDL.addonName, addonLDB, SDL.db.profile.minimap);

  -- register slash commands
  SDL:RegisterChatCommand("sdl", "HandleSlashCommands");

  -- show/hide frame on load
  if self.db.profile.general.loadedStartShown then
    SDL.frame:Show();
    isWindowShown = true;
  else
    SDL.frame:Hide();
    isWindowShown = false;
  end

  -- show/hide welcome message on load
  if self.db.profile.general.loadedWelcomeMessage then
    SDL:Print(string.format("%s %s",
      string.format(SDL.Localization["chat welcome loaded"], SDL.addonTitle, SDL.version),
      string.format(SDL.Localization["chat welcome help"], string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl help"]))
    ));
  end

  -- register addon for LibSharedMedia
	LSM.RegisterCallback(self, "LibSharedMedia_Registered", function(event, type, key)
    if type == "font" and key == SDL.db.global.font.family then
      SDL:UpdateFrame();
    end
	end)
	LSM.RegisterCallback(self, "LibSharedMedia_SetGlobal", function(event, type)
    if type == "font" then
      SDL:UpdateFrame();
    end
	end)
end

function SDL:HandleSlashCommands(input)
  -- Process the slash command ('input' contains whatever follows the slash command)
  input = string.trim(input, " ");

  -- /sdl
  if input == "" or not input then
    SDL:ToggleWindow();

    return;
  end

  -- /sdl about
  if input == "about" then
    SDL:OpenOptions();

    return;
  end

  -- /sdl config
  if input == "config" then
    SDL:OpenOptions();

    return;
  end

  -- /sdl help, /sdl ?
  if input == "help" or input == "?" then
    SDL:Print(SDL.Localization["chat separator"]);
    SDL:Print(SDL.Localization["version"] .. ": " .. SDL.version);
    SDL:Print(SDL.Localization["author"] .. ": " .. SDL.author);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl"]) .. " - " .. SDL.Localization["chat toggle"]);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl about"]) .. " - " .. SDL.Localization["chat about"]);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl config"]) .. " - " .. SDL.Localization["chat config"]);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl help"]) .. " - " .. SDL.Localization["chat help"]);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl minimap"]) .. " - " .. SDL.Localization["chat minimap"]);
    SDL:Print(string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl reset"]) .. " - " .. string.format(SDL.Localization["chat reset"], SDL.db.global.theme.warning.hex));

    return;
  end

  -- /sdl minimap
  if input == "minimap" then
    SDL:Minimap();

    return;
  end

  -- /sdl reset
  if input == "reset" then
    SDL:Reset();

    return;
  end

  if input == "debug" then
    SDL.db.global.debug.debugMode = not SDL.db.global.debug.debugMode;

    SDL:Print(string.format(
      SDL.Localization["chat debug"],
      SDL.db.global.debug.debugMode and SDL.db.global.theme.enabled.hex or SDL.db.global.theme.disabled.hex,
      SDL.db.global.debug.debugMode and SDL.Localization["on"] or SDL.Localization["off"]
    ));

    return;
  end

  if input == "test" then
    SDL:DebugFrame();

    return;
  end

  SDL:Print(string.format(SDL.Localization["chat command error"], string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl help"])));
end

function SDL:OpenOptions()
  -- NOTE: Two calls are needed because it opens interface options but not on the addon's interface options.
  InterfaceOptionsFrame_OpenToCategory(SDL.addonName);
  InterfaceOptionsFrame_OpenToCategory(SDL.addonName);
end

function SDL:IsShown()
  return isWindowShown;
end

function SDL:ToggleWindow()
  if not isWindowShown then
    if SDL.db.profile.general.sounds then
      PlaySound(882);
    end

    SDL.frame:Show();
    isWindowShown = true;
  else
    SDL.frame:Hide();
    isWindowShown = false;
  end
end

function SDL:LabelFormatter(data)
  local str = "";

  if SDL.db.profile.general.textMinimumLevel then
    str = string.format(SDL.Localization["format minimum level"], data.minimum);
  end

  if SDL.db.profile.general.textRecommendedLevels then
    str = str .. string.format(SDL.Localization["format level range"], data.minLevel, data.maxLevel);
  end

  str = str .. string.format(SDL.Localization["format name"], data.name);

  if SDL.db.profile.general.textAbbreviation then
    str = str .. string.format(SDL.Localization["format abbreviation"], data.abbr);
  end

  if SDL.db.profile.general.textLastBossLevel then
    str = str .. string.format(SDL.Localization["format last boss level"], data.lastBossLevel);
  end

  return str;
end

function SDL:LabelColorer(data)
  local playerLevel = 0;
  local targetLevel = 0;

  if SDL.db.global.debug.debugMode then
    playerLevel = SDL.db.global.debug.debugLevel;
  else
    playerLevel = UnitLevel("player");
  end

  if SDL.db.profile.general.colorsBasedOnLastBossLevel then
    targetLevel = data.lastBossLevel;
  else
    targetLevel = math.floor((data.minLevel + data.maxLevel) / 2);
  end

  return unpack(SDL:CalcLevels(playerLevel, targetLevel));
end

function SDL:CalcLevels(playerLevel, targetLevel)
  local alpha = 1;

  if targetLevel >= playerLevel + 10 then
    -- boss
    return { SDL.db.profile.general.colors.boss.red, SDL.db.profile.general.colors.boss.green, SDL.db.profile.general.colors.boss.blue, alpha };
  elseif targetLevel >= playerLevel + 5 then
    -- red
    return { SDL.db.profile.general.colors.red.red, SDL.db.profile.general.colors.red.green, SDL.db.profile.general.colors.red.blue, alpha };
  elseif targetLevel == playerLevel + 3 or targetLevel == playerLevel + 4 then
    -- orange
    return { SDL.db.profile.general.colors.orange.red, SDL.db.profile.general.colors.orange.green, SDL.db.profile.general.colors.orange.blue, alpha };
  elseif targetLevel <= playerLevel + 2 and targetLevel >= playerLevel - 2 then
    -- yellow
    return { SDL.db.profile.general.colors.yellow.red, SDL.db.profile.general.colors.yellow.green, SDL.db.profile.general.colors.yellow.blue, alpha };
  elseif targetLevel <= playerLevel - 3 and targetLevel >= SDL:GrayLevels(playerLevel) then
    -- green
    return { SDL.db.profile.general.colors.green.red, SDL.db.profile.general.colors.green.green, SDL.db.profile.general.colors.green.blue, alpha };
  else
    -- gray
    return { SDL.db.profile.general.colors.gray.red, SDL.db.profile.general.colors.gray.green, SDL.db.profile.general.colors.gray.blue, alpha };
  end
end

function SDL:GrayLevels(playerLevel)
  if playerLevel >= 1 and playerLevel <= 5 then
    return 0;
  elseif playerLevel >= 6 and playerLevel <= 49 then
    return playerLevel - math.floor(playerLevel / 10) - 5;
  elseif playerLevel == 50 then
    return playerLevel - 10;
  elseif playerLevel >= 51 and playerLevel <= 59 then
    return playerLevel - math.floor(playerLevel / 5) - 1;
  elseif playerLevel == 60 then
    return playerLevel - 9;
  end
end

function SDL:CreateLabelHeader()
  local header = AceGUI:Create("Label");
  local str = "";

  if SDL.db.profile.general.textMinimumLevel then
    str = string.format(SDL.Localization["format minimum level"], SDL.Localization["frames general label minimum level"]);
  end

  if SDL.db.profile.general.textRecommendedLevels then
    str = str .. string.format(SDL.Localization["format level range header"], SDL.Localization["frames general label level range"]);
  end

  str = str .. string.format(SDL.Localization["format name"], SDL.Localization["frames general label name"]);

  if SDL.db.profile.general.textAbbreviation then
    str = str .. string.format(SDL.Localization["format abbreviation"], SDL.Localization["frames general label abbreviation"]);
  end

  if SDL.db.profile.general.textLastBossLevel then
    str = str .. string.format(SDL.Localization["format last boss level"], SDL.Localization["frames general label last boss level"]);
  end

  header:SetText(str);
  header:SetFont(LSM:Fetch("font", SDL.db.global.font.family), SDL.db.global.font.size, SDL.db.global.font.style);
  header:SetFullWidth(true);

  return header;
end

function SDL:CreateLabels(data)
  local label = AceGUI:Create("Label");

  label:SetText(SDL:LabelFormatter(data));
  label:SetFullWidth(true);
  label:SetFont(LSM:Fetch("font", SDL.db.global.font.family), SDL.db.global.font.size, SDL.db.global.font.style);

  if SDL.db.profile.general.colorText then
    label:SetColor(SDL:LabelColorer(data));
  end

  return label;
end

function SDL:SetupFrame()
  SDL.frame = SDL.frame or AceGUI:Create("Frame");
  SDL.frame:SetTitle(SDL.addonTitle);
  SDL.frame:SetStatusText(string.format(SDL.Localization["frames general status"], SDL.version, string.format(SDL.Localization["format color"], SDL.db.global.theme.commands.hex, SDL.Localization["commands sdl help"])));
  SDL.frame:SetWidth(SDL.db.global.frame.width);
  SDL.frame:SetHeight(SDL.db.global.frame.height);
  SDL.frame:SetLayout("Fill");
	SDL.frame:ClearAllPoints();
	SDL.frame:SetPoint(unpack(SDL.db.global.frame.position));

  SDL.scrollFrame = SDL.scrollFrame or AceGUI:Create("ScrollFrame");
  SDL.scrollFrame:SetLayout("Flow");
  SDL.frame:AddChild(SDL.scrollFrame);

  SDL:UpdateFrame();

  SDL.frame:SetCallback("OnClose", function()
    isWindowShown = false;
    SDL:SavePosition();
  end);
end

function SDL:UpdateFrame()
  if SDL.scrollFrame.children then
    SDL.scrollFrame:ReleaseChildren();
  end

  SDL.scrollFrame:AddChild(SDL:CreateLabelHeader());

  for index, data in ipairs(SDL.data) do
    if not SDL.db.profile.general.displayRaids and data.raid then
      -- NOTE: There's no "continue" keyword in Lua
    else
      local label = SDL:CreateLabels(data);
      SDL.scrollFrame:AddChild(label);
    end
  end
end

function SDL:Minimap()
  SDL.db.profile.minimap.hide = not SDL.db.profile.minimap.hide;

  if SDL.db.profile.minimap.hide then
    LDBIcon:Hide(SDL.addonName);
  else
    LDBIcon:Show(SDL.addonName);
  end
end

function SDL:Popup()
  StaticPopupDialogs[SDL.addonName .. "WEBSITE"] = {
    text = SDL.Localization["popup"],
    button1 = SDL.Localization["done"],
    OnShow = function(self, data)
      self.editBox:SetText(SDL.about.issues)
      self.editBox:SetWidth(260)
    end,
    hasEditBox = true,
    exclusive = true,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3
  }

  StaticPopup_Show(SDL.addonName .. "WEBSITE")
end

function SDL:SavePosition()
  SDL:Debug(SDL.Localization["saving position"]);

  local point, _, relativePoint, xOffset, yOffset = SDL.frame:GetPoint();
	local position = {point, "UIParent", relativePoint, xOffset, yOffset};

  SDL.db.global.frame.position = position;

	SDL.frame:ClearAllPoints();
	SDL.frame:SetPoint(unpack(SDL.db.global.frame.position));
end


function SDL:Reset()
  for k,v in pairs(SDL.defaults.global) do
    SDL.db.global[k] = v;
  end

  for k,v in pairs(SDL.defaults.profile) do
    SDL.db.profile[k] = v;
  end

  SDL.db.profile.minimap.hide = SDL.defaults.profile.minimap.hide;

  if SDL.db.profile.minimap.hide then
    LDBIcon:Hide(SDL.addonName);
  else
    LDBIcon:Show(SDL.addonName);
  end

  if SDL.db.profile.general.loadedStartShown then
    SDL.frame:Show();
    isWindowShown = true;
  else
    SDL.frame:Hide();
    isWindowShown = false;
  end

  SDL:UpdateFrame();

  SDL:Print(SDL.Localization["chat reset settings"]);
end

-- TODO: Is there a better way of resetting colors? This doesn't work after first reset (changes the colors in the UI).
function SDL:ResetColors()
  for k,v in pairs(SDL.defaults.profile.general.colors) do
    SDL.db.profile.general.colors[k] = v;
  end

  SDL:Debug(SDL.Localization["chat reset colors"])
end

-- debugging
function SDL:Debug(key, value)
  if SDL.db.global.debug.debugMode and SDL.db.global.debug.debugMessages then
    if value ~= nil then
      SDL:Print(tostring(key) .. ": " .. tostring(value));
    else
      SDL:Print(tostring(key));
    end
  end
end

function SDL:DebugSettings(info, value)
  SDL:Debug(info[#info], value);
end

function SDL:DebugFrame()
  if not SDL.db.global.debug.debugMode then return end

  SDL.debugFrame = AceGUI:Create("Frame");
  SDL.debugFrame:SetTitle(SDL.addonTitle .. SDL.Localization["frames debug title"]);
  SDL.debugFrame:SetStatusText(SDL.Localization["frames debug status"]);
  SDL.debugFrame:SetFullWidth(true);
  SDL.debugFrame:SetFullHeight(true);
  SDL.debugFrame:SetLayout("Fill");

  local scroll = scroll or AceGUI:Create("ScrollFrame");
  scroll:SetLayout("Flow");
  SDL.debugFrame:AddChild(scroll);

  for index=1, 60 do
    local label1 = SDL:DebugLabel("playerLevel: 30 - targetLevel: " .. index, SDL:CalcLevels(30, index));
    scroll:AddChild(label1);
  end

  local heading1 = AceGUI:Create("Heading");
  scroll:AddChild(heading1);

  for index=1, 70 do
    local label2 = SDL:DebugLabel("playerLevel: 60 - targetLevel: " .. index, SDL:CalcLevels(60, index));
    scroll:AddChild(label2);
  end
end

function SDL:DebugLabel(text, color)
  local label = AceGUI:Create("Label");

  label:SetText(text);
  label:SetFullWidth(true);
  label:SetFontObject(GameFontHighlight);
  label:SetColor(unpack(color));

  return label;
end
