## Interface: 11304
## Title: Simple Dungeon List
## Notes: Simple list for all dungeons with colored rows based on your level
## Version: v1.0.0
## Author: Saucy (|cffffffffNelaesia|r @ Zandalar Tribe EU)
## DefaultState: Enabled
## SavedVariables: SaucyDB

lib\LibStub\LibStub.lua
lib\CallbackHandler-1.0\CallbackHandler-1.0.lua
lib\AceAddon-3.0\AceAddon-3.0.xml
lib\AceGUI-3.0\AceGUI-3.0.xml
lib\LibSharedMedia-3.0\lib.xml
lib\AceGUI-3.0-SharedMediaWidgets\widget.xml
lib\LibDataBroker-1.1\LibDataBroker-1.1.lua
lib\LibDBIcon-1.0\lib.xml

core\init.lua

locale\enUS.lua

core\config.lua
core\core.lua
