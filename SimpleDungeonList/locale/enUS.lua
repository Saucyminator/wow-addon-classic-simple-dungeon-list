local Localization = LibStub("AceLocale-3.0"):NewLocale("SDL", "enUS", true)

if Localization then
-- commands
Localization["commands sdl"] = "/sdl"
Localization["commands sdl about"] = "/sdl about"
Localization["commands sdl config"] = "/sdl config"
Localization["commands sdl help"] = "/sdl help"
Localization["commands sdl minimap"] = "/sdl minimap"
Localization["commands sdl reset"] = "/sdl reset"


-- chat
Localization["chat welcome loaded"] = "%s (%s) loaded!"
Localization["chat welcome help"] = "Type %s for more information."
Localization["chat separator"] = "--------------------"
Localization["chat toggle"] = "Show/hide the window."
Localization["chat about"] = "Show information about the addon."
Localization["chat config"] = "Show addon config menu."
Localization["chat help"] = "Show addon help."
Localization["chat minimap"] = "Show/hide the minimap icon."
Localization["chat reset"] = "|cff%sResets addon's settings.|r"
Localization["chat reset settings"] = "Settings reset."
Localization["chat reset colors"] = "Colors reset."
Localization["chat debug"] = "Debug mode is |cff%s%s|r"
Localization["chat command error"] = "Invalid command. For a list of options please type: %s"


-- frames
-- frames/general
Localization["frames general status"] = "%s - Type %s for more information."
Localization["frames general label minimum level"] = "Minimum level"
Localization["frames general label level range"] = "Level range"
Localization["frames general label name"] = "Name"
Localization["frames general label abbreviation"] = "Abbreviation"
Localization["frames general label last boss level"] = "Last boss level"

-- frames/debug
Localization["frames debug title"] = " - Debug frame"
Localization["frames debug status"] = "Debug frame status"


-- settings
-- settings/intro
Localization["settings description"] = "Version %s"

-- settings/general
Localization["settings tab general"] = "General"
Localization["settings general loaded header"] = "Startup settings"
Localization["settings general loaded welcome message"] = "Display welcome message when logging in."
Localization["settings general loaded welcome message desc"] = ""
Localization["settings general loaded start shown"] = "Display the dungeon list window when logging in."
Localization["settings general loaded start shown desc"] = ""
Localization["settings general colors header"] = "Color settings"
Localization["settings general colors row"] = "Color each row depending on your level."
Localization["settings general colors row desc"] = ""
Localization["settings general colors based on last boss level"] = "Base the coloring of each row on the last boss' level."
Localization["settings general colors based on last boss level desc"] = "Otherwise it bases the coloring on the average level in the zone."
Localization["settings general colors level colors"] = "Level colors"
Localization["settings general colors level colors desc"] = ""
Localization["settings general colors gray"] = "Gray"
Localization["settings general colors gray desc"] = ""
Localization["settings general colors green"] = "Green"
Localization["settings general colors green desc"] = ""
Localization["settings general colors yellow"] = "Yellow"
Localization["settings general colors yellow desc"] = ""
Localization["settings general colors orange"] = "Orange"
Localization["settings general colors orange desc"] = ""
Localization["settings general colors red"] = "Red"
Localization["settings general colors red desc"] = ""
Localization["settings general colors boss"] = "Boss"
Localization["settings general colors boss desc"] = ""
Localization["settings general colors reset"] = "Reset colors"
Localization["settings general colors reset desc"] = "/reload is needed afterwards."
Localization["settings general text header"] = "Text style and information settings"
Localization["settings general text abbreviation"] = "Show dungeon abbreviation."
Localization["settings general text abbreviation desc"] = "Example: DM for The Deadmines, BRD for Blackrock Depths, etc..."
Localization["settings general text last boss level"] = "Show the last boss' level."
Localization["settings general text last boss level desc"] = ""
Localization["settings general text minimum level"] = "Show minimum level to enter."
Localization["settings general text minimum level desc"] = ""
Localization["settings general text recommended levels"] = "Show recommended levels."
Localization["settings general text recommended levels desc"] = ""
Localization["settings general display rows header"] = "Text row settings"
Localization["settings general display raids"] = "Display raid dungeons."
Localization["settings general display raids desc"] = "20-man and 40-man raids."
Localization["settings general sounds header"] = "Sound settings"
Localization["settings general sounds"] = "Play sound effects."
Localization["settings general sounds desc"] = ""
Localization["settings general font header"] = "Font settings"
Localization["settings general font family"] = "Font family."
Localization["settings general font family desc"] = ""
Localization["settings general font size"] = "Font size."
Localization["settings general font size desc"] = ""
Localization["settings general font outline"] = "Font outline."
Localization["settings general font outline desc"] = ""
Localization["settings general font shadow"] = "Font shadow."
Localization["settings general font shadow desc"] = ""
Localization["settings general minimap header"] = "Minimap settings"
Localization["settings general minimap"] = "Enable Minimap icon"
Localization["settings general minimap desc"] = ""
Localization["settings general minimap tooltip left click"] = "Show/hide the main frame"
Localization["settings general minimap tooltip right click"] = "Open options"
Localization["settings general minimap tooltip drag"] = "Move minimap button"

-- settings/about
Localization["settings tab about"] = "About"
Localization["settings about header"] = "About"
Localization["settings about"] = "Author: %s\n\nAny constructive criticism is welcomed!\nHappy dungeon hunting, gamer!"
Localization["settings about desc"] = ""
Localization["settings about known issues header"] = "Known issues"
Localization["settings about known issues"] = "%s"
Localization["settings about known issues desc"] = ""
Localization["settings about issues header"] = "Report bugs and issues"
Localization["settings about issues"] = "Report bugs/issues at:\n%s"
Localization["settings about issues desc"] = ""
Localization["settings about issues button report"] = "Report an issue"
Localization["settings about issues button report desc"] = "Opens a in-game popup so you can copy the URL."

-- settings/debug
Localization["settings tab debug"] = "Debug"
Localization["settings debug header"] = "Debug settings"
Localization["settings debug mode"] = "Turn debug mode on/off."
Localization["settings debug mode desc"] = ""
Localization["settings debug chat messages"] = "Display debug chat messages."
Localization["settings debug chat messages desc"] = ""
Localization["settings debug test level header"] = "Test level"
Localization["settings debug test level"] = "Toggle level to test."
Localization["settings debug test level desc"] = ""
Localization["settings debug test level range"] = "Which level to test."
Localization["settings debug test level range desc"] = ""
Localization["settings debug reset header"] = "Reset"
Localization["settings debug reset"] = "Reset all settings"
Localization["settings debug reset desc"] = "Resets all settings used by the addon."

-- settings/changelog
Localization["settings tab changelog"] = "Changelog"
Localization["settings changelog"] = "%s"
Localization["settings changelog desc"] = ""


-- misc words
Localization["on"] = "On"
Localization["off"] = "Off"
Localization["left click"] = "Left-click"
Localization["right click"] = "Right-click"
Localization["drag"] = "Drag"
Localization["drag and drop"] = "Drag & drop"
Localization["popup"] = "Copy the url and paste it on your browser"
Localization["done"] = "Done"
Localization["author"] = "Author"
Localization["version"] = "Version"
Localization["saving position"] = "Saving position"


-- formatting
Localization["format color"] = "|cFF%s%s|r"
Localization["format color key"] = "|cFF%s%s:|r %s"
Localization["format color value"] = "%s: |cFF%s%s|r"
Localization["format color key value"] = "|cFF%s%s:|r |cFF%s%s|r"
Localization["format minimum level"] = "[%s] - "
Localization["format level range header"] = "%s - "
Localization["format level range"] = "%s-%s - "
Localization["format name"] = "%s"
Localization["format abbreviation"] = " (%s)"
Localization["format last boss level"] = " - %s"


-- dungeons
Localization["dungeons rfc"] = "Ragefire Chasm"
Localization["dungeons rfc abbr"] = "RFC"
Localization["dungeons rfc location"] = "Orgrimmar"

Localization["dungeons wc"] = "Wailing Caverns"
Localization["dungeons wc abbr"] = "WC"
Localization["dungeons wc location"] = "The Barrens"

Localization["dungeons dm"] = "The Deadmines"
Localization["dungeons dm abbr"] = "DM"
Localization["dungeons dm location"] = "Westfall"

Localization["dungeons sfk"] = "Shadowfang Keep"
Localization["dungeons sfk abbr"] = "SFK"
Localization["dungeons sfk location"] = "Silverpine Forest"

Localization["dungeons bfd"] = "Blackfathom Deeps"
Localization["dungeons bfd abbr"] = "BFD"
Localization["dungeons bfd location"] = "Ashenvale"

Localization["dungeons stockade"] = "The Stockade"
Localization["dungeons stockade abbr"] = "Stockade"
Localization["dungeons stockade location"] = "Stormwind"

Localization["dungeons gnomer"] = "Gnomeregan"
Localization["dungeons gnomer abbr"] = "Gnomer"
Localization["dungeons gnomer location"] = "Dun Morogh"

Localization["dungeons rfk"] = "Razorfen Kraul"
Localization["dungeons rfk abbr"] = "RFK"
Localization["dungeons rfk location"] = "The Barrens"

Localization["dungeons sm gy"] = "The Scarlet Monastery (Graveyard)"
Localization["dungeons sm gy abbr"] = "SM GY"
Localization["dungeons sm gy location"] = "Tirisfal Glades"

Localization["dungeons sm lib"] = "The Scarlet Monastery (Library)"
Localization["dungeons sm lib abbr"] = "SM LIB"
Localization["dungeons sm lib location"] = "Tirisfal Glades"

Localization["dungeons sm arms"] = "The Scarlet Monastery (Armory)"
Localization["dungeons sm arms abbr"] = "SM ARMS"
Localization["dungeons sm arms location"] = "Tirisfal Glades"

Localization["dungeons sm cath"] = "The Scarlet Monastery (Cathedral)"
Localization["dungeons sm cath abbr"] = "SM CATH"
Localization["dungeons sm cath location"] = "Tirisfal Glades"

Localization["dungeons rfd"] = "Razorfen Downs"
Localization["dungeons rfd abbr"] = "RFD"
Localization["dungeons rfd location"] = "The Barrens"

Localization["dungeons ulda"] = "Uldaman"
Localization["dungeons ulda abbr"] = "ULDA"
Localization["dungeons ulda location"] = "Badlands"

Localization["dungeons zf"] = "Zul'Farrak"
Localization["dungeons zf abbr"] = "ZF"
Localization["dungeons zf location"] = "Tanaris"

Localization["dungeons mara orange"] = "Maraudon (Orange)"
Localization["dungeons mara orange abbr"] = "MARA ORANGE"
Localization["dungeons mara orange location"] = "Desolace"

Localization["dungeons mara purple"] = "Maraudon (Purple)"
Localization["dungeons mara purple abbr"] = "MARA PURPLE"
Localization["dungeons mara purple location"] = "Desolace"

Localization["dungeons mara poison falls"] = "Maraudon (Poison Falls)"
Localization["dungeons mara poison falls abbr"] = "MARA POISON FALLS"
Localization["dungeons mara poison falls location"] = "Desolace"

Localization["dungeons mara inner"] = "Maraudon (Inner)"
Localization["dungeons mara inner abbr"] = "MARA INNER"
Localization["dungeons mara inner location"] = "Desolace"

Localization["dungeons st"] = "Temple of Atal'Hakkar"
Localization["dungeons st abbr"] = "ST"
Localization["dungeons st location"] = "Swamp of Sorrows"

Localization["dungeons brd"] = "Blackrock Depths"
Localization["dungeons brd abbr"] = "BRD"
Localization["dungeons brd location"] = "Searing Gorge/Burning Steppes"

Localization["dungeons lbrs"] = "Lower Blackrock Spire"
Localization["dungeons lbrs abbr"] = "LBRS"
Localization["dungeons lbrs location"] = "Searing Gorge/Burning Steppes"

Localization["dungeons ubrs"] = "Upper Blackrock Spire"
Localization["dungeons ubrs abbr"] = "UBRS"
Localization["dungeons ubrs location"] = "Searing Gorge/Burning Steppes"

Localization["dungeons dm east"] = "Dire Maul (East)"
Localization["dungeons dm east abbr"] = "DM EAST"
Localization["dungeons dm east location"] = "Feralas"

Localization["dungeons dm north"] = "Dire Maul (North)"
Localization["dungeons dm north abbr"] = "DM NORTH"
Localization["dungeons dm north location"] = "Feralas"

Localization["dungeons dm west"] = "Dire Maul (West)"
Localization["dungeons dm west abbr"] = "DM WEST"
Localization["dungeons dm west location"] = "Feralas"

Localization["dungeons scholo"] = "Scholomance"
Localization["dungeons scholo abbr"] = "SCHOLO"
Localization["dungeons scholo location"] = "Western Plaguelands"

Localization["dungeons strat living"] = "Stratholme (Main Gate, living)"
Localization["dungeons strat living abbr"] = "STRAT LIVING"
Localization["dungeons strat living location"] = "Eastern Plaguelands"

Localization["dungeons strat undead"] = "Stratholme (Service Gate, undead)"
Localization["dungeons strat undead abbr"] = "STRAT UNDEAD"
Localization["dungeons strat undead location"] = "Eastern Plaguelands"

-- dungeons/raids
Localization["dungeons ony"] = "Onyxia's Lair"
Localization["dungeons ony abbr"] = "ONY"
Localization["dungeons ony location"] = "Dustwallow Marsh"

Localization["dungeons zg"] = "Zul'Gurub"
Localization["dungeons zg abbr"] = "ZG"
Localization["dungeons zg location"] = "Stranglethorn Vale"

Localization["dungeons mc"] = "Molten Core"
Localization["dungeons mc abbr"] = "MC"
Localization["dungeons mc location"] = "Searing Gorge/Burning Steppes"

Localization["dungeons bwl"] = "Blackwing Lair"
Localization["dungeons bwl abbr"] = "BWL"
Localization["dungeons bwl location"] = "Searing Gorge/Burning Steppes"

Localization["dungeons aq20"] = "Ruins of Ahn'Qiraj"
Localization["dungeons aq20 abbr"] = "AQ20"
Localization["dungeons aq20 location"] = "Silithus"

Localization["dungeons aq40"] = "Temple of Ahn'Qiraj"
Localization["dungeons aq40 abbr"] = "AQ40"
Localization["dungeons aq40 location"] = "Silithus"

Localization["dungeons naxx"] = "Naxxramas"
Localization["dungeons naxx abbr"] = "NAXX"
Localization["dungeons naxx location"] = "LOCATION"
end
