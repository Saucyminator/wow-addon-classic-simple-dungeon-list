config:

- include raid dungeons
- show/hide * level dungeons
  * gray
  * green
  * yellow
  * orange
  * red
  * boss
- show colors
- show recommended levels
- show last boss level
- change order ascending/descending
- enable/disable welcome message





TODO:s:
- have a minimap icon?
- have the two * list addons able to act on their own, but if they are both found they become TABS in WOWACE frame




Dungeon list:

level range, dungeon name, last boss level, abbreviation

13-18 - Ragefire Chasm - 16 - RFC
15-25 - Wailing Caverns - 22 - WC
18-23 - The Deadmines - 21 - DM
22-30 - Shadowfang Keep - 26 - SFK
20-30 - Blackfathom Deeps - 28 - BFD
22-30 - The Stockade - 29 - Stockade
24-34 - Gnomeregan - 34 - Gnomer
30-40 - Razorfen Kraul - 33 - RFK
26-45 - The Scarlet Monastery (Graveyard) - 34 - SM GY
26-45 - The Scarlet Monastery (Library) - 37 - SM LIB
26-45 - The Scarlet Monastery (Armory) - 40 - SM ARMS
26-45 - The Scarlet Monastery (Cathedral) - 42 - SM CATH
40-50 - Razorfen Downs - 41 - RFD
35-45 - Uldaman - 47 - Ulda
44-54 - Zul'Farrak - 48 - ZF
46-55 - Maraudon (Orange) - 48 - Maraudon
46-55 - Maraudon (Purple) - 47 - Maraudon
46-55 - Maraudon (Poison Falls) - 49 - Maraudon
46-55 - Maraudon (Inner) - 51 - Maraudon
55-60 - Temple of Atal'Hakkar - 55 - ST
52-60 - Blackrock Depths - 59 - BRD
55-60 - Blackrock Spire (Lower) - 60 - LBRS
55-60 - Blackrock Spire (Upper) - 62 - UBRS
55-60 - Dire Maul (East) - Boss - DM East
55-60 - Dire Maul (North) - Boss - DM North
55-60 - Dire Maul (West) - Boss - DM West
58-60 - Scholomance - 61 - Scholo
58-60 - Stratholme (Main Gate, living) - Boss - Strat living
58-60 - Stratholme (Service Gate, undead) - 62 - Strat undead
60+ --- Onyxia's Lair - Boss - ONY
60+ --- Zul'Gurub - Boss - ZG
60+ --- Molten Core - Boss - MC
60+ --- Blackwing Lair - Boss - BWL
60+ --- Ruins of Ahn'Qiraj - Boss - AQ20
60+ --- Temple of Ahn'Qiraj - Boss - AQ40
60+ --- Naxxramas - Boss - Naxx
